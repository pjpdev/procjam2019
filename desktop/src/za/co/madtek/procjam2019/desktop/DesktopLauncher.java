package za.co.madtek.procjam2019.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import za.co.madtek.procjam2019.MyGdxGame;

import java.io.FileInputStream;
import java.util.Properties;

public class DesktopLauncher {
	public static void main (String[] arg) throws Exception {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "#PROCJAM 2019";
		config.width = 1280;
		config.height = 720;
		//config.resizable = false;
		new LwjglApplication(new MyGdxGame(), config);
	}
}
